from flask import Flask, request
from flask_cors import CORS
import pandas as pd
import numpy as np
import pygmsh
from waveguide import Waveguide
from sklearn.ensemble import RandomForestClassifier
from scipy.interpolate import griddata
import matplotlib.pyplot as plt

app = Flask(__name__)
CORS(app)

# Default parameters for rectangle/circle
XMIN = 0
XMAX = 2
YMIN = 0
YMAX = XMAX/2
Z = 0


def build_training_data():
    """Returns analytical waveguide solution data for TEmn modes where M = 0..5 and N = 0..5
    
    Returns:
        X_train -- [description]
    """
    def Ex(m, n, X, Y):
        a = XMAX
        b = YMAX

        return n/b*np.multiply(np.cos(m*np.pi*X/a), np.sin(n*np.pi*Y/b))

    def Ey(m, n, X, Y):
        a = XMAX
        b = YMAX

        return -m/a*np.multiply(np.sin(m*np.pi*X/a), np.cos(n*np.pi*Y/b))

    X_train = None
    y_train = None
    for m in range(6):
        for n in range(6):
            x = np.linspace(XMIN, XMAX, num=25)
            y = np.linspace(YMIN, YMAX, num=25)

            (X, Y) = np.meshgrid(x, y)
            ex = np.abs(Ex(m, n, X, Y))
            ey = np.abs(Ey(m, n, X, Y))

            if X_train is None:
                X_train = np.hstack(
                    (ex.flatten(), ey.flatten())).reshape(1, -1)
                if np.max(X_train) != 0 and np.max(x_train) > 1e-3:
                    X_train = X_train/np.max(X_train)
                y_train = np.array([float(str(m)+str(n))])
            else:
                x_train = np.hstack(
                    (ex.flatten(), ey.flatten())).reshape(1, -1)
                if np.max(x_train) != 0 and np.max(x_train) > 1e-3:
                    x_train = x_train/np.max(x_train)
                X_train = np.vstack((X_train, x_train))
                y_train = np.append(y_train, [float(str(m)+str(n))])

    return (np.abs(X_train), y_train)


# Train with Analytical data
(X, y) = build_training_data()
print('Training classifier...')
clf = RandomForestClassifier(n_estimators=100, max_features=None)
clf.fit(X, y)
print('Classifier trained!')


@app.route("/demo", methods=['GET'])
def Solve():
    geom = pygmsh.built_in.Geometry()

    if request.args.get('type', '') == 'circ':
        print('Requested Circ')
        geom.add_circle([XMAX, XMAX, 0.0], XMAX/2)
    elif request.args.get('type', '') == 'rect':
        print('Requested Rect')
        geom.add_rectangle(XMIN, XMAX, YMIN, YMAX, Z)
    elif request.args.get('type','') == 'H':
        print('Requested Double-Ridged WG')
        geom.add_polygon([
            [ 0.0/3.0,  0.0/3.0, 0.0],
            [ 1.0/3.0,  0.0/3.0, 0.0],
            [ 1.0/3.0,  1.0/3.0, 0.0],
            [ 2.0/3.0,  1.0/3.0, 0.0],
            [ 2.0/3.0,  0.0/3.0, 0.0],
            [ 3.0/3.0,  0.0/3.0, 0.0],
            [ 3.0/3.0,  3.0/3.0, 0.0],
            [ 2.0/3.0,  3.0/3.0, 0.0],
            [ 2.0/3.0,  2.0/3.0, 0.0],
            [ 1.0/3.0,  2.0/3.0, 0.0],
            [ 1.0/3.0,  3.0/3.0, 0.0],
            [ 0.0/3.0,  3.0/3.0, 0.0],
            ]
            )
    else:
        print('Invalid: Requested' + request.args.get('type',''))
        return None

    points, cells, point_data, cell_data, field_data = pygmsh.generate_mesh(
        geom, prune_z_0=True, extra_gmsh_arguments=['-clmax', '0.1'], geo_filename='wg.geo')

    wg = Waveguide(points, cells)
    wg.assembleGlobalMatrix()
    w, v = wg.solve()
    fields = wg.computeFields()

    centers = wg.getCenters()
    X = centers[:, 0]
    Y = centers[:, 1]

    MODE = int(request.args.get('mode'))
    vec = fields[:, 0, MODE]+1j*fields[:, 1, MODE]

    if request.args.get('type', '') == 'rect':
        pred = clf.predict(interp_data(X, Y, fields[:, 0, MODE], fields[:, 1, MODE]))
        pred = 'TE' + str(int(pred[0])).zfill(2)
        print(pred)
    else:
        pred = 'N/A'
        print(pred)

    df = pd.DataFrame(data={'X': X, 'Y': Y, 'mag': np.abs(
        vec), 'ang': 180/np.pi*np.angle(vec)+90})

    return df.to_json()[0:-1] + ',"prediction":"{}"}}'.format(pred)


def interp_data(x, y, ex, ey):
    x_new = np.linspace(XMIN, XMAX, num=25)
    y_new = np.linspace(YMIN, YMAX, num=25)
    (X_new, Y_new) = np.meshgrid(x_new, y_new)

    points = np.vstack((x.flatten(), y.flatten())).T
    points_new = np.vstack((X_new.flatten(), Y_new.flatten())).T

    # Interpolate Ex/Ey to [n,] length ndarrays
    ex_new = griddata(points, ex.flatten(), points_new,
                      method='nearest').reshape(X_new.shape)
    ey_new = griddata(points, ey.flatten(), points_new,
                      method='nearest').reshape(X_new.shape)

    # Convert to absolute values
    ex_new = np.abs(ex_new)
    ey_new = np.abs(ey_new)

    if np.max(ex_new) > 1e-3:
        ex_new = ex_new/np.max(ex_new)
    if np.max(ey_new) > 1e-3:
        ey_new = ey_new/np.max(ey_new)

    return np.concatenate((ex_new.flatten(), ey_new.flatten())).reshape(1, -1)
