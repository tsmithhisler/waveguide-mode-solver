import React from 'react'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import { withStyles } from '@material-ui/core/styles'
import HighchartsReact from 'highcharts-react-official'
import Highcharts from 'highcharts'

// Load vector module
require('highcharts/modules/vector')(Highcharts)

const waveguides = [{
  label: 'Rectangular',
  value: 'rect'
},
{
  label: 'Circular',
  value: 'circ'
},
{
  label: 'Double Ridged',
  value: 'H'
}]

/* Used to create the empty multidimensional array that holds the vector data */
function createArray(length) {
  var arr = new Array(length || 0),
    i = length;

  if (arguments.length > 1) {
    var args = Array.prototype.slice.call(arguments, 1);
    while (i--) arr[length - 1 - i] = createArray.apply(this, args);
  }

  return arr;
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  grid: {
    textAlign: 'center',
    alignItems: 'center'
  },
  button: {
    marginRight: theme.spacing.unit,
    marginLeft: theme.spacing.unit,
    minWidth: '200px',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  paper: {
    marginRight: 'auto',
    marginLeft: 'auto',
    paddingBottom: theme.spacing.unit,
    paddingTop: theme.spacing.unit,
    width: 500,
  }
});

// Highcharts Vector Field Component
class QuiverPlot extends React.Component {
  constructor() {
    super(); // Needed to call 'this' in constructor

    this.state = {
      type: 'rect',
      mode: '',
      sklearnPrediction: 'N/A',
      options: {
        title: {
          text: 'Waveguide Transverse Electric Field'
        },
        legend: {
          enabled: false,
        },
        xAxis: {
          min: 0,
          max: 100,
          gridLineWidth: 1,
        },
        yAxis: {
          min: 0,
          max: 100,
          title: {
            text: undefined,
          },
        },
        series: [{
          type: 'vector',
          name: 'E-Field',
          color: Highcharts.getOptions().colors[1],
          data: [],
        }]
      }
    };
  }

  // Handle selection change for mode selection
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    },() => {this.requestSolution()});
  };

  // Perform HTTP Get request with flask application
  requestSolution = (e) => {
    fetch(`http://127.0.0.1:5000/demo?type=${this.state.type}&mode=${this.state.mode}`)
    .then(response => response.json()) // returns promise that resolves with result of parsing body of response as JSON
    .then(response => this.updateSeries(response))
  }


  updateSeries = (data) => {
    // Create empty array on [n_val x 4] shape
    let arr = createArray(Object.keys(data.X).length, 4);

    // Parse X/Y, Mag/Ang values into 4d array
    for (var key in Object.keys(data.X)) {
      arr[parseInt(key)][0] = data.X[key]
      arr[parseInt(key)][1] = data.Y[key]
      arr[parseInt(key)][2] = data.mag[key]
      arr[parseInt(key)][3] = data.ang[key]
    }

    // Update state variables of chart with data parsed from input as well as update axis limits
    this.setState({
      options: {
        series: [{
          data: arr
        }],
        xAxis: {
          min: Math.min.apply(null, Object.values(data.X)),
          max: Math.max.apply(null, Object.values(data.X)),
          gridLineWidth: 1
        },
        yAxis: {
          min: Math.min.apply(null, Object.values(data.Y)),
          max: Math.max.apply(null, Object.values(data.Y)),
        },
      },
      sklearnPrediction: data.prediction,
    });
  }

  render() {
    const { options } = this.state; // Plot options used by highcharts
    const { classes } = this.props; // React component specific options

    return (
      <Grid container spacing={24}>
        <Grid item xs={12} className={classes.grid}>
          <HighchartsReact
            highcharts={Highcharts}
            constructorType={'chart'}
            options={options}
            containerProps={{ style: {height: "400px"} }}
          />
        </Grid>
        <Grid item xs={12} className={classes.grid}>
          <Paper className={classes.paper}>
            <TextField
              id="mode-selection"
              select
              label="Mode Selection"
              variant="outlined"
              value={this.state.mode}
              margin="dense"
              onChange={this.handleChange('mode')}
              className={classes.textField}
              helperText="Please select the mode to display">
              {[...Array(9).keys()].map(option => (
                <MenuItem key={option} value={option}>
                  {option.toString()}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              id="solve-selection"
              select
              label="Waveguide Selection"
              variant="outlined"
              value={this.state.type}
              margin="dense"
              onChange={this.handleChange('type')}
              className={classes.textField}
              helperText="Please select the type of waveguide">
              {waveguides.map(option => (
                <MenuItem key={option.label} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
            <TextField
              id="sklearn-pred"
              label="ML Prediction"
              variant="outlined"
              value={this.state.sklearnPrediction}
              margin="dense"
              className={classes.textField}
              helperText="Rectangular prediction only">
            </TextField>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(QuiverPlot);